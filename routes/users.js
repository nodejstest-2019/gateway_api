const proxy = require('koa-proxy');
const authMiddleware = require('../lib/middleware/auth')

module.exports = (router) => {
    const ENDPOINT = '/users'

    router
        .get(ENDPOINT, authMiddleware, proxy({
            host: process.env.USERS_SERVICE
        }))
}
