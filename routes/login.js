const bodyParser = require('koa-bodyparser');
const handler = require('../controllers/login');

module.exports = (router) => {
    const ENDPOINT = '/login'

    router.post(ENDPOINT, bodyParser(),ctx => handler.login(ctx))
}
