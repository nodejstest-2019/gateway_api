const proxy = require('koa-proxy');

module.exports = (router) => {
    const ENDPOINT = '/signup'

    router.post(ENDPOINT, proxy({
        url: process.env.USERS_SERVICE + '/api/v1/users'
    }))
}
