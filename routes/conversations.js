const proxy = require('koa-proxy');
const bodyParser = require('koa-bodyparser');
const handler = require('../controllers/conversations');
const authMiddleware = require('../lib/middleware/auth')

module.exports = (router) => {
    const ENDPOINT = '/conversations'

    router
        .post(ENDPOINT, authMiddleware, bodyParser(), ctx => handler.createConversation(ctx))
        .get(ENDPOINT, authMiddleware, ctx => handler.listConversations(ctx))
        .get('ENDPOINT/:conversation_id', authMiddleware, ctx => handler.conversationDetails(ctx))
        .post('ENDPOINT/:conversation_id', authMiddleware, bodyParser(), ctx => handler.sendMessage(ctx))

}
