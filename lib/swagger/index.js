const serve = require('koa-static');
const mount = require('koa-mount');
const basicAuth = require('koa-basic-auth');

const setSwagger = app => {
    const swaggerUiAssetPath = require('swagger-ui-dist').getAbsoluteFSPath();
    app.use(mount(`/api-docs/v1`, async (ctx, next) => {
        try {
            await next()
        } catch (err) {
            if (401 === err.status) {
                ctx.status = 401
                ctx.set('WWW-Authenticate', 'Basic')
                ctx.body = 'Please enter your credentials to explore swagger :)'
            } else {
                throw err
            }
        }
    }))
    app.use(mount(`/api-docs/v1`, basicAuth({ name: process.env.SWAGGER_USERNAME, pass: process.env.SWAGGER_PASSWORD })))
    app.use(mount(`/api-docs/v1`, serve(swaggerUiAssetPath)))
    app.use(mount(`/api-docs/swagger/v1`, serve(__dirname)))
}

module.exports = setSwagger
