const { jwtHelper } = require('../helpers/index')

const authMiddleware = async (ctx, next) => {
    if (!ctx.request.header.authorization) {
        ctx.status = 401
        ctx.body = {
            error: {
                status: ctx.status,
                title: 'Unauthorized',
                message: 'Token not provided'
            }
        }
    } else {
        const token = ctx.request.header.authorization.split(' ')[1]
        const decoded = jwtHelper.verify(token, ctx.jwtPublicKey);
        if (!decoded) {
            ctx.status = 401
            ctx.body = {
                error: {
                    status: ctx.status,
                    title: 'Unauthorized',
                    message: 'Token not valid',
                }
            }
        } else {
            ctx.state.token = decoded
            return next()
        }
    }
}
module.exports = authMiddleware
