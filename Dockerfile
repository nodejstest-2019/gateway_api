FROM node:10-alpine
# Labels
LABEL MAINTAINER="Salar Hafezi <salar.hfz@gmail.com>"
# Create app directory
WORKDIR /usr/src/app
# Install deps
COPY package*.json yarn.lock ./
RUN yarn
# Bundle app source
COPY . .
EXPOSE 4000
CMD ["node", "index.js"]
