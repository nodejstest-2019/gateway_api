# Nodejs Test - Gateway API by Salar Hafezi <salar.hfz@gmail.com>

## Important notes:


*  Some variable names has changed (createdAt -> created_at, updatedAt -> updated_at)
*  Images of Users and Chats APIs are live on the docker hub under my profile -> salarhafezi
*  I'm preparing swagger for this project.