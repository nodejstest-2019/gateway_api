const Koa = require('koa');
const cors = require('@koa/cors');
const helmet = require('koa-helmet');

const generalErrorHandler = require('./lib/middleware/error_handler');
const setRoutes = require('./routes');
const setSwagger = require('./lib/swagger/index');
// // server info
const PORT = parseInt(process.env.PORT) || 4000;

const app = new Koa();
if (process.env.NODE_ENV === 'dev') {
  // load .envs
  require('dotenv').config();
  app.use(require('koa-logger')());
}

app.use(cors());
app.use(helmet());
// set priv/pub keys
app.context.jwtPrivateKey = process.env.JWT_PRIVATE_KEY.replace(/\\n/g, '\n');
app.context.jwtPublicKey = process.env.JWT_PUBLIC_KEY.replace(/\\n/g, '\n');
// general exception handler
app.use(generalErrorHandler);
// set routes
setRoutes(app);
// setup swagger
setSwagger(app);
// bootstrap the app
const server = app.listen(PORT, () => {
  console.log(`app is running on port ${PORT}`);
});

module.exports = {
  server,
};
