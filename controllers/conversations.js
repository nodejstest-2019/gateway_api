const request = require('request-promise-native');
const Joi = require('@hapi/joi')

exports.createConversation = async ctx => {
    try {
        const token = ctx.state.token
        const { sub: from } = token
        const { with: to } = ctx.request.body
        const validationResult = Joi.string().min(4).required().validate(to)
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            try {
                const newConversation = await request({
                    uri: `${process.env.CHATS_SERVICE}/api/v1/conversations`,
                    method: 'POST',
                    json: true,
                    body: { parties: [from, to] }
                })
                const secondParty = await request({
                    uri: `${process.env.USERS_SERVICE}/api/v1/users/${to}`,
                    method: 'GET',
                    json: true,
                })
                const parties = [{
                    id: token.sub,
                    name: token.user_name,
                    email: token.user_email,
                    created_at: token.user_created_at
                },
                {
                    id: secondParty.data.id,
                    name: secondParty.data.name,
                    email: secondParty.data.email,
                    created_at: secondParty.data.created_at
                }]
                ctx.status = 201
                ctx.body = {
                    data: {
                        id: newConversation.data.id,
                        parties: parties,
                        created_at: newConversation.data.created_at,
                        updated_at: newConversation.data.updated_at
                    }
                }
            } catch (err) {
                ctx.status = err.statusCode;
                ctx.body = err.response.body;
            }
        }

    } catch (err) {
        throw new Error(err)
    }
}

exports.listConversations = async ctx => {
    try {
        const token = ctx.state.token
        const { sub } = token
        const { offset, limit } = ctx.query
        try {
            const data = []
            const response = await request({
                uri: `${process.env.CHATS_SERVICE}/api/v1/conversations`,
                method: 'GET',
                qs: {
                    offset,
                    limit,
                    user_id: sub
                },
                json: true
            })
            const conversations = response.data
            for (let conversation of conversations) {
                const to = conversation.parties.filter(el => el != sub)[0]
                const secondParty = await request({
                    uri: `${process.env.USERS_SERVICE}/api/v1/users/${to}`,
                    method: 'GET',
                    json: true,
                })
                data.push({
                    id: conversation.id,
                    with: {
                        id: secondParty.data.id,
                        name: secondParty.data.name,
                        email: secondParty.data.email,
                        created_at: secondParty.data.created_at
                    },
                    created_at: conversation.created_at,
                    updated_at: conversation.updated_at,
                    lastMessage: conversation.lastMessage
                })
            }
            ctx.status = 200
            ctx.body = {
                data: data
            }
        } catch (err) {
            ctx.status = err.statusCode;
            ctx.body = err.response.body;
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

exports.conversationDetails = async ctx => {
    try {
        const token = ctx.state.token
        const { sub } = token
        const { offset, limit } = ctx.query
        const { conversation_id } = ctx.params
        try {
            const response = await request({
                uri: `${process.env.CHATS_SERVICE}/api/v1/conversations/${conversation_id}`,
                method: 'GET',
                qs: {
                    offset,
                    limit
                },
                json: true
            })
            const conversation = response.data
            const to = conversation.parties.filter(el => el != sub)[0]
            const secondParty = await request({
                uri: `${process.env.USERS_SERVICE}/api/v1/users/${to}`,
                method: 'GET',
                json: true,
            })
            const data = {
                id: conversation.id,
                with: {
                    id: secondParty.data.id,
                    name: secondParty.data.name,
                    email: secondParty.data.email,
                    created_at: secondParty.data.created_at
                },
                created_at: conversation.created_at,
                updated_at: conversation.updated_at,
                messages: conversation.messages
            }
            ctx.status = 200
            ctx.body = {
                data: data
            }
        } catch (err) {
            ctx.status = err.statusCode;
            ctx.body = err.response.body;
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

exports.sendMessage = async ctx => {
    try {
        const token = ctx.state.token
        const { sub } = token
        const { conversation_id } = ctx.params
        const { body } = ctx.request.body
        try {
            const newMessage = await request({
                uri: `${process.env.CHATS_SERVICE}/api/v1/conversations/${conversation_id}`,
                method: 'POST',
                json: true,
                body: { from: sub, body: body }
            })
            ctx.status = 201
            ctx.body = {
                data: newMessage.data
            }
        } catch (err) {
            ctx.status = err.statusCode;
            ctx.body = err.response.body;
        }
    } catch (err) {
        throw new Error(err.message)
    }
}