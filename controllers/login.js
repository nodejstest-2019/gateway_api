const Joi = require('@hapi/joi');
const request = require('request-promise-native');
const { jwtHelper } = require('../lib/helpers');

exports.login = async ctx => {
    try {
        const { email } = ctx.request.body
        if (email) {
            const validationResult = Joi.string().email().required().validate(email)
            if (validationResult.error) {
                const error = {
                    status: 400,
                    title: 'Validation failed',
                    message: validationResult.error.message
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            } else {
                try {
                    const user = await request({
                        uri: `${process.env.USERS_SERVICE}/api/v1/users`,
                        method: 'GET',
                        qs: { email: email },
                        json: true
                    })
                    const token = jwtHelper.sign({
                        iss: 'nodejsTest',
                        sub: user.data.id,
                        iat: Date.now(),
                        user_email: email,
                        user_name: user.data.name,
                        user_created_at: user.data.created_at
                    }, ctx.jwtPrivateKey)
                    if (token && token.length > 0) {
                        ctx.status = 200
                        ctx.body = {
                            data: {
                                ...user.data,
                                token: token
                            }
                        }
                    } else {
                        const error = {
                            status: 500,
                            title: 'Internal server error',
                            message: 'Token generation failed'
                        }
                        ctx.status = error.status
                        ctx.body = {
                            error: error
                        }
                    }
                } catch (err) {
                    ctx.status = err.statusCode;
                    ctx.body = err.response.body;
                }
            }
        } else {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: 'No email address provided'
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        }
    } catch (err) {
        throw new Error(err)
    }
}
